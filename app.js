/*
to start with nodemon:
nodemon --exec npm start
*/

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
require('dotenv').config();

var api = require('./routes/api');
var open = require('./routes/open');

var jwt = require('jsonwebtoken');
var config = require('./config');

var app = express();

var allowCrossDomain = function(req, res, next) {
		res.header('Access-Control-Allow-Origin', '*');
		res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
		res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, jwt-token');

		// intercept OPTIONS method
		if ('OPTIONS' == req.method) {
			res.sendStatus(200);
		}
		else {
			next();
		}
};

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /open
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(allowCrossDomain);

// Parse body to into JSON
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', open);

app.use(function(req, res, next) {
	var token = req.body.token || req.query.token || req.headers['jwt-token'];
	if (token) {
		jwt.verify(token, 'secret', function (err, decoded) {
			if (err) {
				return res.status(403).send({
					success: false,
					message: 'Failed to Authenticate Token'
				});
			} else {
				req.decoded = decoded;
				next();
			}
		});
	} else {
		return res.status(403).send({
			success: false,
			message: 'No Token Provided'
		});
	}
});
app.use('/api', api);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handler
app.use(function(err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render('error');
});

module.exports = app;
