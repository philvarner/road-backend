var promise = require('bluebird');
var config = require('../config');

var options = {
  // Initialization Options
  promiseLib: promise
};

var pgp = require('pg-promise')(options);

const connectionString = process.env.DATABASE_URL !== undefined ? process.env.DATABASE_URL :  {
                                                                                  host: 'localhost',
                                                                                  port: 5432,
                                                                                  database: 'roads'
                                                                                };


const db = pgp(connectionString);

module.exports = {
  db
};
