var dbo = require('./database-object');
var db = dbo.db;
var logger = require('morgan');
var jwt = require('jsonwebtoken');
const crypto = require('crypto');

function authenticate(req, res, next) {
	db.one('SELECT email, password, salt, iterations FROM user_account WHERE email = $1', req.body.email)
		.then(function (data) {
			if (!validatePassword(data.password, req.body.password, data.salt, data.iterations)) {
				res.status(403)
					.json({
						status:'Forbidden',
						message: 'Invalid Password or Email'
					});
			} else {
			    db.one('SELECT user_id, first_name, last_name, email FROM user_account WHERE email = $1', [req.body.email])
					.then(function (data) {
						var token = jwt.sign(data, 'secret', {
													expiresIn: 1209600 // <-- 14 days
												});
						res.status(200)
							.json({
								status: 'Success',
								data: data,
								token: token,
								message: 'Authentication Success'
							});
					})
					.catch(function (error) {
						return next(error);
					});
				}
		})
		.catch(function (err) {
			// console.log(err);
			return res.status(401)
				.json({
					err: err
				});
		});
}

function renewAuth(req, res, next) {
		db.any('SELECT user_id, first_name, last_name, email FROM user_account WHERE email = $1', [req.decoded.email])
		.then(function (data) {
			var jwtoken = jwt.sign(data[0], 'secret', {
										expiresIn: 1209600 // <-- 14 days //604800 // expires in 7 days(in seconds)
									});
			return res.status(200)
				.json({
					status: 'Authentication Credentials Updated',
					token: jwtoken,
					data: data[0]
				});
		})
		.catch(function (error) {
			return next(error);
		});
}

function validatePassword(savedHash, passwordAttempt, savedSalt, savedIterations) {
	var result = savedHash === crypto.pbkdf2Sync(passwordAttempt, savedSalt, savedIterations, 64, 'sha256').toString('base64');
	return result
}

function hashSaltPassword(password) {
	var salt = crypto.randomBytes(128).toString('base64');
	var iterations = 10000;
	var hash = crypto.pbkdf2Sync(password, salt, iterations, 64, 'sha256').toString('base64');

	return {
		salt: salt,
		iterations: iterations,
		hash: hash
	};
}

function createUser(req, res, next) {
	db.none('SELECT email FROM user_account WHERE email = $1', [req.body.email])
	.then(function () {
		encryption = hashSaltPassword(req.body.password);
		req.body.password = encryption.hash;
		req.body.salt = encryption.salt;
		req.body.iterations = encryption.iterations;
		req.body.username = req.body.email;

			db.one('INSERT INTO user_account(first_name, last_name, email, username, password, salt, iterations)' +
					'VALUES(${first_name}, ${last_name}, ${email}, ${username}, ${password}, ${salt}, ${iterations})' +
					'RETURNING user_id, username, email, first_name, last_name',
					req.body)
			.then(function (user) {
				var token = jwt.sign(user, 'secret', {
											expiresIn: 1209600 // <-- 14 days //604800 // expires in 7 days(in seconds)
										});
				res.status(200)
					.json({
						status: 'success',
						data: user,
						token: token,
						message: 'Inserted one user'
					});
			})
			.catch(function (error) {
				return next(error);
			});
		})
		.catch(function (err) {
			res.status(209).json({
				success: false,
				message: 'This email is unavailable.'
			});
		});
}

function resetPasswordEmail(req, res, next) {
	var encryption = hashSaltPassword(req.body.password);
	db.none('UPDATE user_account SET password=$1, salt=$2, iterations=$3 WHERE user_id=$4',
			[encryption.hash, encryption.salt, encryption.iterations, req.decoded.user_id])
			.then(function () {
				res.status(200)
					.json({
						status: 'success',
						success: true,
						message: 'Updated user'
					});
			})
			.catch(function (error) {
				return next(error);
			})}

function sendPasswordResetEmail(req, res, next) {
	db.one('SELECT user_id, email FROM user_account WHERE email = $1', req.body.email)
		.then(function (data) {

			var resetToken = jwt.sign(data, 'secret', {
										expiresIn: 600 // expires in 600 seconds (10 minutes)
									});

			// Email to user for password reset
      // const msg = {
      //   to: req.body.email,
      //   from: 'support@roadtracker.com',
      //   subject: 'Road Tracker - Password Reset',
      //   text: 'You requested a password reset.\n' +
			// 				'Follow this link to reset your password now:\n\n' +
			// 				req.headers.origin + '/password-reset/' + resetToken.toString() +
			// 				'\n\nThank you!'
      // };
      // sgMail.send(msg);
			res.status(200)
				.json({
					status: 'success',
					success: true,
					message: 'If this account exists, reset instructions will be sent to the given email'
				});

		})
		.catch(function (error) {
			next(error);
		});
}

module.exports = {
	renewAuth: renewAuth,
	authenticate: authenticate,
	createUser: createUser,
	resetPasswordEmail: resetPasswordEmail,
	sendPasswordResetEmail: sendPasswordResetEmail,
};
