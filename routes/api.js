var express = require('express');
var router = express.Router();

var userDB = require('../queries/user');

router.get('/', function(req, res, next) {
	res.send({ Error: 'There is no resource at this route.' });
});

router.post('/user', userDB.createUser);
router.post('/verifyJWT', userDB.renewAuth);
router.put('/email-update-password', userDB.resetPasswordEmail);

module.exports = router;
