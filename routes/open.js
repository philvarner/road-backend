var express = require('express');
var router = express.Router();

var userDB = require('../queries/user');

router.get('/', function(req, res, next) {
	res.send('Please specify a public resource.');
});

router.post('/authenticate', userDB.authenticate);
router.post('/send-password-reset', userDB.sendPasswordResetEmail);
router.post('/create-account', userDB.createUser);

module.exports = router;
